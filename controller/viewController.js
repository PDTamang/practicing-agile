const path = require('path') // path module provides a way of working directories and file paths

// LOG IN PAGE
exports.getLoginForm = (req, res) => {
    res.sendFile(path.join(__dirname, '../','view', 'login.html'))
}

// SIGN UP FORM
exports.getSignupForm = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'view', 'signup.html'))
}

// HOME PAGE
exports.getHome = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'view', 'dashboard.html'))
}
// sendFile() to serve the static files